#pragma once
#include <BLRevive/Component.h>
#include <BLRevive/Detours/FunctionDetour.h>
#include <nlohmann/json.hpp>

#define NLOHMANN_FROM_TYPE_NON_INTRUSIVE(Type, ...) \
	inline void from_json(const nlohmann::json& nlohmann_json_j, Type& nlohmann_json_t) { NLOHMANN_JSON_EXPAND(NLOHMANN_JSON_PASTE(NLOHMANN_JSON_FROM, __VA_ARGS__)) }
#define NLOHMANN_TO_TYPE_NON_INTRUSIVE(Type, ...) \
	inline void to_json(nlohmann::json& nlohmann_json_j, const Type& nlohmann_json_t) { NLOHMANN_JSON_EXPAND(NLOHMANN_JSON_PASTE(NLOHMANN_JSON_TO, __VA_ARGS__)) }

void from_json(const nlohmann::json& j, FSettingsData& data);
void to_json(nlohmann::json& j, const FSettingsData data);

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(FSettingsProperty,
	PropertyId, Data, AdvertisementType);
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(FOnlineProfileSetting,
	Owner, ProfileSetting);

namespace BLRE
{
	struct KeyBinding
	{
		std::string primary;
		std::string alternate;

	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(KeyBinding, primary, alternate);

	class SettingsManager : Component
	{
	public:
		SettingsManager() : Component("SettingsManager") {}

		void Init();

		void LoadSettings();
		std::vector<FOnlineProfileSetting> LoadProfileSettings();

		void SaveSettings();
		void SaveProfileSettings();

		void UpdateProfileSettings(UFoxProfileSettings* profileSettings);

		static inline std::string KeyBindingConfigFileName = "keybinding.json";
		static inline std::string ProfileFileName = "UE3_online_profile.json";

	protected:
		std::vector<FOnlineProfileSetting> profileSettings = {};
		std::string profileName = "Player";
		std::filesystem::path profileDirPath = "";

		//void ApplySettings();
		void ApplyProfileSettings(UFoxProfileSettings* settings);

		void DETOUR_CB_CLS(RegisterCustomPlayerDataStoresCb, AFoxPC, RegisterCustomPlayerDataStores);
		void DETOUR_CB_CLS(SettingsApplyChangesDt, UFoxSettingsUIBase, ei_ApplyChanges);
		void DETOUR_CB_CLS(HackQuitGameOnQuit, UFoxMenuUI, ei_QuitMatch);
		void DETOUR_CB_CLS(HackQuitGameOnIdleKick, AFoxPC, ClientKickedForIdle);
	};
}